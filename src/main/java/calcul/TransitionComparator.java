package calcul;

import grammaire.Grammaire;

import java.util.Comparator;

public class TransitionComparator implements Comparator<Transition> {
    @Override
    public int compare(Transition t1, Transition t2) {
        if (t1.getItemId() < t2.getItemId()){
            return -1;
        }
        if (t1.getItemId() > t2.getItemId()) {
            return 1;
        }
        else {
            if (t1.getTerminal().equals(t2.getTerminal())) {
                return 0;
            }
            if (t1.getTerminal().equals(Grammaire.firstNonTerminal)) {
                return -1;
            }
            if (t2.getTerminal().equals(Grammaire.firstNonTerminal)) {
                return 1;
            }
            else{
                return t1.getTerminal().getValue().compareTo(t2.getTerminal().getValue());
            }
        }
    }
}
