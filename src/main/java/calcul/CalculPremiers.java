package calcul;

import grammaire.GrammaireFormatter;

import java.util.*;

public class CalculPremiers {

    Map<String, List<String>> grammaire;
    public CalculPremiers(List<String> grammaireBrut){
        this.grammaire = GrammaireFormatter.format(grammaireBrut);
    }

    public Map<String, Set<String>> getAllPremiers(){
        Map<String, Set<String>> premiers = new HashMap<>();
        for (String nonTerminal : this.grammaire.keySet()){
            premiers.put(nonTerminal, getPremiers(nonTerminal));
        }
        return premiers;
    }

    public Set<String> getPremiers(String nonTerminal){
        Set<String> premiers = new HashSet<>();
        for (String regle : this.grammaire.get(nonTerminal)){
            char value2 = regle.charAt(0);
            // Si le char est un non terminal, on doit ajouter les premiers de celui ci
            if (Character.isUpperCase(value2)) {
                // si le char n'est pas le non terminal pour lequel on cherche ses premiers
                // Exemple S -> Aa est correct
                // Exemple S -> Sa est donc mauvais -> boucle infinie
                if (!(value2+"").equals(nonTerminal)){
                    premiers = getPremiers(value2 + "");
                }
            }
            // Sinon, le terminal choisi est un premier
            else{
                premiers.add(value2 + "");
            }
        }
        return premiers;
    }
}
