package grammaire;

import java.util.*;

import calcul.Regle;
import calcul.Symbole;
import exceptions.*;

public class Grammaire {
    // Classe formattant une grammaire et calculant ses premiers et suivants.
    public static final Symbole DOLLAR = new Symbole("$", true);
    public static final Symbole EPSYLONE = new Symbole("ε", true);

    private List<String> grammaireBrut;
    private Set<Symbole> langage;
    private Map<Symbole, List<Regle>> grammaire;
    private Map<Symbole, Set<Symbole>> premiers, suivants;
    private final List<Symbole> nonTerminauxVisites;
    public static Symbole firstNonTerminal, APOSTROPHE;

    public Grammaire(List<String> langageBrut, List<String> grammaireBrut) throws SyntaxArrowMissing {
        this.grammaireBrut = grammaireBrut;
        this.nonTerminauxVisites = new ArrayList<>();
        firstNonTerminal = null;
        APOSTROPHE = null;
        LangageBrutToList(langageBrut);
        GrammaireBrutToMap(grammaireBrut);
        CalculAllPremiers();
        CalculSuivant();
    }

    public Map<Symbole, List<Regle>> getGrammaire() {
        return grammaire;
    }

    public Map<Symbole, Set<Symbole>> getPremiers() {
        return premiers;
    }

    public Map<Symbole, Set<Symbole>> getSuivants() {
        return suivants;
    }

    public List<String> getGrammaireBrut(){
        return this.grammaireBrut;
    }

    public Set<Symbole> getLangage(){
        return this.langage;
    }

    public void CalculSuivant() {
        this.suivants = new HashMap<>();
        for (Symbole symbole : grammaire.keySet()) {
            if (symbole.isNonTerminal()) {
                suivants.putIfAbsent(symbole, new HashSet<>());
                List<Regle> regles = grammaire.get(symbole);
                if (symbole.equals(firstNonTerminal)) {
                    suivants.get(firstNonTerminal).add(DOLLAR);
                    nonTerminauxVisites.add(firstNonTerminal);
                }
                for (Regle regle : regles) {
                    Symbole finDeChaine = regle.getFinChaine();
                    gestionMarqueurFinDeChaine(symbole, finDeChaine);
                }
                for (Regle regle : regles) {
                    // on separe la cle de sa valeur en separant en plus chaque valeur si un "|" est present
                    for (int i = 0; i < regle.getRegle().size(); i++) {
                        Symbole symbole1 = regle.getRegle().get(i);
                        if (symbole1.isNonTerminal()) {
                            for (Regle regle1 : grammaire.get(symbole1)) {
                                Regle regle2 = new Regle(regle.getRegle());
                                regle2.replaceSymbole(symbole1, regle1);
                                for (int j = 0; j < regle2.getRegle().size() - 1; j++) {
                                    Symbole symbole2 = regle2.getRegle().get(j);
                                    Symbole symbole3 = regle2.getRegle().get(j + 1);
                                    if (symbole2.isNonTerminal()) {
                                        suivants.putIfAbsent(symbole2, new HashSet<>());
                                        if (symbole3.isTerminal() && !symbole3.equals(EPSYLONE)) {
                                            suivants.get(symbole2).add(symbole3);
                                        } else if (symbole3.isNonTerminal()) {
                                            suivants.get(symbole2).addAll(this.premiers.get(symbole3));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void gestionMarqueurFinDeChaine(Symbole key, Symbole finDeChaine) {
        if (finDeChaine.equals(EPSYLONE)) {
            for (Regle regle : grammaire.get(firstNonTerminal)) {
                Symbole fin = regle.getFinChaine();
                if (fin.equals(key)) {
                    suivants.putIfAbsent(fin, new HashSet<>());
                    suivants.get(fin).add(DOLLAR);
                }
            }
        } else if (finDeChaine.isNonTerminal() && !nonTerminauxVisites.contains(finDeChaine)) {
            nonTerminauxVisites.add(finDeChaine);
            for (Regle regle : grammaire.get(finDeChaine)) {
                Symbole symbole = regle.getFinChaine();
                if (symbole.isNonTerminal() && !nonTerminauxVisites.contains(symbole)) {
                    gestionMarqueurFinDeChaine(finDeChaine, symbole);
                } else if (symbole.isTerminal() && !symbole.equals(EPSYLONE)) {
                    suivants.putIfAbsent(finDeChaine, new HashSet<>());
                    suivants.get(finDeChaine).add(DOLLAR);
                }
            }
        }
    }

    public void CalculAllPremiers() {
        this.premiers = new HashMap<>();
        for (Symbole symbole : this.grammaire.keySet()) {
            premiers.put(symbole, CalculPremier(symbole, new HashSet<>()));
        }
    }

    public Set<Symbole> CalculPremier(Symbole nonTerminal, Set<Symbole> visited) {
        visited.add(nonTerminal);
        Set<Symbole> premiers = new HashSet<>();
        for (Regle regle : this.grammaire.get(nonTerminal)) {
            Symbole premier = regle.getDebutChaine();
            // Si le char est un non terminal, on doit ajouter les premiers de celui ci
            if (premier.isNonTerminal()) {
                // le char n'est pas le non terminal pour lequel on cherche ses premiers
                // Exemple S -> Aa est correct
                // Exemple S -> Sa est donc mauvais -> récursion infinie
                if (!(premier).equals(nonTerminal)) {
                    if (!visited.contains(premier)) {
                        premiers = CalculPremier(premier, visited);
                    }
                }
            }
            // Sinon, le terminal choisi est un premier
            else if (!premier.equals(EPSYLONE)) {
                premiers.add(premier);
            }
        }
        return premiers;
    }

    private void LangageBrutToList(List<String> langageBrut) {
        this.langage = new HashSet<>();
        for (String line : langageBrut){
            line = line.replaceAll(" ", "");
            String[] values = line.split(",");
            for (String value : values) {
                this.langage.add(new Symbole(value, true));
            }
        }
    }

    private void GrammaireBrutToMap(List<String> grammaireBrut) throws SyntaxArrowMissing {
        // transforme un language ecrit sous forme reelle en une structure de donnees specifique
        this.grammaire = new HashMap<>();
        List<String> newGrammaireBrut = new ArrayList<>();
        try {
            for (String line : grammaireBrut) {
                // on separe les deux elements de la ligne
                if (!line.isBlank()){
                    String[] separation = line.split("->");
                    Symbole key = new Symbole(separation[0].replaceAll(" ", ""), false);
                    if (firstNonTerminal == null) {
                        firstNonTerminal = key;
                        APOSTROPHE = new Symbole(key.getValue() + "'", false);
                    }
                    String regle = separation[1];
                    String[] regles = regle.split("\\|");
                    List<Regle> reglesRes = new ArrayList<>();
                    for (String r : regles) {
                        List<Symbole> symboles = new ArrayList<>();
                        String[] split = r.split(" ");
                        for (String element : split) {
                            if (element.length() > 0) {
                                boolean terminal = true;
                                for (char c : element.toCharArray()) {
                                    if (Character.isUpperCase(c)) {
                                        terminal = false;
                                    }
                                }
                                symboles.add(new Symbole(element, terminal));
                            }
                        }
                        reglesRes.add(new Regle(symboles));
                    }
                    // on ajoute les valeurs aux terminaux correspondants
                    this.grammaire.putIfAbsent(key, new ArrayList<>());
                    this.grammaire.get(key).addAll(reglesRes);
                    newGrammaireBrut.add(line);
                }
                this.grammaireBrut = newGrammaireBrut;
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            throw new SyntaxArrowMissing("Error syntax, doit être écrit sous la forme 'nonTerminal->regle' Example: S->Sa");
        }
    }

    public String toString(){
        StringBuilder res = new StringBuilder();
        for (String s : grammaireBrut){
            res.append(s).append("\n");
        }
        return res.toString();
    }
}
