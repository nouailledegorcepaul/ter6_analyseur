package calcul;

import grammaire.Grammaire;
import java.util.*;

import static grammaire.Grammaire.APOSTROPHE;
import static grammaire.Grammaire.EPSYLONE;

public class CalculCollectionLR0 implements CalculCollection{

    public Grammaire grammaire;
    public Map<Integer, Set<ItemPoint>> items;
    public Map<Symbole, Set<ItemPoint>> baseItems;
    public Map<Transition, Integer> transitions;
    public int itemIndex, itemIndexToAdd;
    private final boolean conflict;
    private boolean working;

    public CalculCollectionLR0(Grammaire grammaire, boolean conflict){
        // grammaire: la version formattée et utilisable pour le calcul de la collection
        // itemIndex: le numéro d'item sur lequel on travaille
        // itemIndexToAdd: le numéro du nouvel item à ajouter
        // items: le dictionnaire stockant tous les items calculés, ayant pour clé son numero d'item et pour valeur l'ensemble de ses règles
        // baseItems: le dictionnaire stockant toutes les règles de base d'un non terminal, ayant pour clé le non terminal et pour valeur ses règles
        // transitions: le dictionnaire stockant toutes les transitions calculées, ayant pour clé une transition, et pour valeur le numero d'item associé à sa valeur
        // conflict: boolean précisant s'il faut s'arréter si un conflit est trouvé
        this.grammaire = grammaire;
        this.itemIndex = 0;
        this.itemIndexToAdd = 0;
        this.items = new HashMap<>();
        this.baseItems = new HashMap<>();
        this.transitions = new TreeMap<>(new TransitionComparator());
        this.conflict = conflict;
        this.working = true;
    }

    @Override
    public Map<Integer, Set<ItemPoint>> getItems() {
        return items;
    }

    @Override
    public Map<Transition, Integer> getTransitions() {
        return transitions;
    }

    @Override
    public boolean hasFoundConflict() {
        return !working;
    }

    @Override
    public void calculateAllItems(){
        // on initie les règles de base de chaque symbole non terminal
        for (Symbole symbole : this.grammaire.getGrammaire().keySet()){
            initItems(symbole);
        }
        // initialise le premier item de la collection avec la grammaire augmentée S' -> S
        initI0();
        // itemIndex initialisé à 0 puisqu'on commence par calculer les items de I0
        this.itemIndex = 0;
        // itemIndex initialisé à 1 puisque I0 existe déjà, il faut donner calculer I1
        this.itemIndexToAdd = 1;
        calculItems();

//        System.out.println("---------------------");
//        for (Transition tr : this.transitions.keySet()){
//            System.out.println(tr + "-> I" + this.transitions.get(tr));
//        }
//        System.out.println(this.working);
    }

    private void initItems(Symbole symbole){
        // Stocke dans une map ayant pour clé un Symbole non terminal et pour valeur un Set de ItemPoint.
        // les règles de base d'un non terminal
        this.baseItems.put(symbole, new HashSet<>());
        for (Regle regle : this.grammaire.getGrammaire().get(symbole)){
            this.baseItems.get(symbole).add(new ItemPointLR0(symbole, regle));
        }
    }

    private void initI0(){
        // On ajoute la grammaire augmentée S'->S aux règles de base de S
        // pour créer le premier item I0
        this.items.put(0, new HashSet<>());
        Symbole initSymbole = APOSTROPHE;
        List<Symbole> symboles = new ArrayList<>();
        symboles.add(Grammaire.firstNonTerminal);
        Regle initRegle = new Regle(symboles);
        this.items.get(0).add(new ItemPointLR0(initSymbole, initRegle));
        this.items.get(0).addAll(this.baseItems.get(Grammaire.firstNonTerminal));

        // itemPoinsToAdd stocke l'ensemble des non terminaux à ajouter s'ils sont
        // à analyser directement après.
        Set<ItemPoint> itemPointsToAdd = new HashSet<>();
        for (ItemPoint itemPoint : this.items.get(0)){
            if (itemPoint.hasSuivant()){
                Symbole symboleSuivant = itemPoint.getSuivant();
                // Si le suivant est un non terminal
                if (symboleSuivant.isNonTerminal()){
                    // On ajoute toutes ces règles de base aux règles à ajouter
                    itemPointsToAdd.addAll(this.baseItems.get(symboleSuivant));
                }
            }
        }
        Transition res = new Transition(0, EPSYLONE, itemPointsToAdd);
        if (this.conflict){
            checkConflict(res);
        }
        // On ajoute les potentielles règles à ajouter à l'item I0
        this.items.get(0).addAll(itemPointsToAdd);
    }

    private void calculItems(){
        // On initialise un ensemble de String ayant un paramètre de tri qui tri dans l'ordre alphabétique décroissant
        // On y ajoute l'ensemble des suivants
        List<Symbole> suivants = new ArrayList<>(getSuivants(this.itemIndex));
        // Pour chaque suivant, on calcul sa transition
        for (Symbole suivant : suivants){
            if (this.working) {
                Transition transition = getTransition(this.itemIndex, suivant);
                if (this.working) {
                    boolean alreadyIn = false;
                    // Après avoir obtenu la transition, on vérifie si ses valeurs n'appartiennent pas déjà
                    // à un item de la collection d'item
                    for (int index : this.items.keySet()) {
                        // Si les valeurs de transition sont déjà présentes, on l'ajoute aux transitions
                        // avec pour valeur l'indice de l'item ayant ces memes valeurs.
                        if (this.items.get(index).equals(transition.getValue())) {
                            this.transitions.put(transition, index);
                            alreadyIn = true;
                            break;
                        }
                    }
                    if (!alreadyIn) {
                        // Si les valeurs de transitions ne sont pas déjà présentes, on l'ajoute aux transitions
                        // avec pour valeur le nouvel indice d'item à ajouter
                        // On ajoute ensuite le nouvel item aux items.
                        this.transitions.put(transition, this.itemIndexToAdd);
                        this.items.put(this.itemIndexToAdd, transition.getValue());
                        // On incrémente l'indice d'item à ajouter.
                        this.itemIndexToAdd++;
                    }
                }
                else{
                    this.transitions.put(transition, this.itemIndexToAdd);
                    this.items.put(this.itemIndexToAdd, transition.getValue());
                    // On incrémente l'indice d'item à ajouter.
                    this.itemIndexToAdd++;
                }
            }
        }
        // On calcul les nouveaux items en prenant connaissance de l'item suivant
        if (this.itemIndex < this.itemIndexToAdd - 1 && this.working){
            this.itemIndex++;
            calculItems();
        }
    }

    private List<Symbole> getSuivants(int itemNum){
        Set<Symbole> suivants = new TreeSet<>(Comparator.comparing(String::valueOf));
        // Pour chaque règle, on ajoute son suivant à l'ensemble s'il en a un
        for (ItemPoint ip : this.items.get(itemNum)){
            if (ip.hasSuivant()){
                if (!ip.getSuivant().equals(EPSYLONE)){
                    suivants.add(ip.getSuivant());
                }
            }
        }
        List<Symbole> listSuivants = new ArrayList<>(suivants);
        if (listSuivants.contains(Grammaire.firstNonTerminal)){
            List<Symbole> sortedSuivants = new ArrayList<>();
            sortedSuivants.add(Grammaire.firstNonTerminal);
            for (Symbole s : listSuivants){
                if (!s.equals(Grammaire.firstNonTerminal)){
                    sortedSuivants.add(s);
                }
            }
            return sortedSuivants;
        }
        return listSuivants;
    }

    private Transition getTransition(int itemNum, Symbole suivant){
        Set<ItemPoint> itemPoints = new HashSet<>();
        // Pour chaque regle, si son suivant est égal à celui donné en paramètre
        // on crée une copie de l'objet, on met à jour son point d'analyse et on l'ajoute
        // à l'ensemble des nouvelles regles.
        for (ItemPoint i : this.items.get(itemNum)){
            if (i.hasSuivant()){
                if (i.getSuivant().equals(suivant)) {
                    ItemPoint i2 = new ItemPointLR0(i);
                    // avancement du point d'analyse
                    i2.update();
                    itemPoints.add(i2);
                }
            }
        }
        Set<Symbole> nonTerminaux = new HashSet<>();
        // Pour chaque nouvelle règle à ajouter on doit ajouter les règles de base de leurs suivants
        // s'ils sont des non terminaux
        for (ItemPoint i : itemPoints){
            if (i.hasSuivant()){
                // S'il a un suivant et que c'est un non terminal
                if (i.getSuivant().isNonTerminal()){
                    // On ajoute son suivant (non terminal) à l'ensemble des non terminaux
                    nonTerminaux.add(i.getSuivant());
                }
            }
        }

        // On ajoute toutes les règles des non terminaux suivants
        for (Symbole nt : nonTerminaux){
            itemPoints.addAll(this.baseItems.get(nt));
            boolean test = true;
            Set<ItemPoint> setToDevelop = this.baseItems.get(nt);
            while(test){
                Set<ItemPoint> nextSetToDevelop = new HashSet<>();
                for (ItemPoint itemPoint : setToDevelop){
                    if (itemPoint.hasSuivant()){
                        if (itemPoint.getSuivant().isNonTerminal()){
                            for (ItemPoint itemPoint2 : this.baseItems.get(itemPoint.getSuivant())){
                                if (!itemPoints.contains(itemPoint2)){
                                    itemPoints.add(itemPoint2);
                                    nextSetToDevelop.add(itemPoint2);
                                }
                            }
                        }
                    }
                }
                if (nextSetToDevelop.size()>0){
                    setToDevelop = nextSetToDevelop;
                }
                else{
                    test = false;
                }
            }
        }
        Transition res = new Transition(itemNum, suivant, itemPoints);
        if (this.conflict){
            checkConflict(res);
        }
        return res;
    }

    private void checkConflict(Transition transition){
        // Met à jour le boolean working qui permet de préciser si on calcule le reste de la collection.
        // On test si la transition donnée en paramètre présente ou non des conflits.

        // On stocke tous les items finis dans un ensemble
        Set<ItemPoint> endItems = new HashSet<>();
        for (ItemPoint item : transition.getValue()){
            if (item.fini()){
                endItems.add(item);
            }
        }
        Set<Symbole> terminaux = new HashSet<>();
        for (ItemPoint item : endItems){
            if (!item.getNonTerminal().equals(APOSTROPHE) && item.fini()){
                Set<Symbole> suivs = this.grammaire.getSuivants().get(item.getNonTerminal());
                for (ItemPoint item2 : transition.getValue()){
                    if (!item2.fini()){
                        Symbole next = item2.getRegle().getSymbole(item2.getPosition());
                        if (suivs.contains(next)){
                            System.out.println("Conflict réduire décaler: " + transition.getTerminal() +", "+item);
                            working = false;
                        }
                    }
                }
            }

            if (this.grammaire.getSuivants().containsKey(item.getNonTerminal())){
                for (Symbole end : this.grammaire.getSuivants().get(item.getNonTerminal())){
                    if (terminaux.contains(end)){
                        System.out.println("Conflict réduire réduire: " + transition.getTerminal() +", "+item + ", " + end);
                        working = false;
                    }
                }
                terminaux.addAll(this.grammaire.getSuivants().get(item.getNonTerminal()));
            }
        }
    }
}
