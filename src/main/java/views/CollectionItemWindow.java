package views;

import calcul.*;
import controller.Controller;
import grammaire.Grammaire;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.io.IOException;
import java.util.*;

public class CollectionItemWindow implements InteractiveView{

    private Scene scene;
    private Controller controller;

    @FXML
    public BorderPane scrollPaneScene;
    public VBox itemsVbox;
    public Button nextItemButton;
    public Button previousItemButton;
    public Button resetItemButton;
    private CalculCollection collection;
    private List<Label> itemsLabel;
    private int itemIndex;


    public static CollectionItemWindow creerVue() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(CollectionItemWindow.class.getClassLoader().getResource("floatItems.fxml"));
        fxmlLoader.load();
        CollectionItemWindow floatWindow = fxmlLoader.getController();
        floatWindow.initialiserVue();
        return floatWindow;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public Scene getScene() {
        return scene;
    }

    private void initialiserVue() {
        this.scene = new Scene(scrollPaneScene, 640, 480);
    }

    public void writeData(Grammaire grammaire, String type, boolean conflict){
        this.itemsLabel = new ArrayList<>();
        this.itemsVbox.getChildren().clear();

        switch (type) {
            case "LR1" -> this.collection = new CalculCollectionLR1(grammaire, conflict);
            case "LR0" -> this.collection = new CalculCollectionLR0(grammaire, conflict);
        }
        this.collection.calculateAllItems();
        writeItems();
    }
    public void writeItems(){
        Map<Integer, Set<ItemPoint>> items = this.collection.getItems();
        StringBuilder item0String = new StringBuilder("I0 = {");
        List<ItemPoint> l = items.get(0).stream().toList();
        for (int i = 0; i<l.size(); i++){
            item0String.append(l.get(i));
            if (i < l.size()-1) {
                item0String.append(", ");
            }
        }
        item0String.append("}");

        Label item0 = new Label(item0String.toString());
        item0.setFont(new Font(14.0));
        this.itemsLabel.add(item0);

        int currentItem = 0;
        Map<Transition, Integer> transitions = this.collection.getTransitions();
        for (Transition transition : transitions.keySet()){
            Label label = new Label();
            if (transitions.get(transition) > currentItem){
                label.setText("I"+transitions.get(transition) + " = " + transition.detailedToString());
                currentItem++;
            }
            else{
                label.setText(transition + " = I" + transitions.get(transition));
            }
            label.setFont(new Font(14.0));
            label.setMinWidth(Region.USE_PREF_SIZE);
            this.itemsLabel.add(label);
        }
        startItems();
    }

    public void nextItem(){
        if (this.itemIndex < this.itemsLabel.size()){
            this.itemsVbox.getChildren().add(this.itemsLabel.get(this.itemIndex));
            this.itemIndex++;

            this.nextItemButton.setDisable(this.itemIndex == this.itemsLabel.size());
            this.previousItemButton.setDisable(this.itemIndex == 1);
        }
    }

    public void previousItem(){
        if (this.itemIndex > 1){
            this.itemsVbox.getChildren().remove(this.itemIndex-1);
            this.itemIndex--;

            this.previousItemButton.setDisable(this.itemIndex == 1);
            this.nextItemButton.setDisable(this.itemIndex == this.itemsLabel.size());
        }
    }

    public void startItems(){
        this.itemIndex = 0;
        this.itemsVbox.getChildren().clear();
        this.nextItemButton.setDisable(false);
        this.previousItemButton.setDisable(true);
        nextItem();
    }

    public void toutVoir(KeyEvent keyEvent) {
        if (Objects.equals(keyEvent.getText(), "t")) {
            itemsVbox.getChildren().clear();
            for (Label label : itemsLabel) {
                itemsVbox.getChildren().add(label);
            }
            itemIndex = itemsLabel.size();
            this.nextItemButton.setDisable(true);
            this.previousItemButton.setDisable(false);
        }
    }
}
