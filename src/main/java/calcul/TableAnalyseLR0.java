package calcul;

import grammaire.Grammaire;

import java.util.*;

import static grammaire.Grammaire.*;

public class TableAnalyseLR0 implements TableAnalyse{

    private Map<Symbole, Set<Symbole>> suivants;
    private Map<Integer, Set<ItemPoint>> items;
    private Map<Transition, Integer> transitions;
    private Map<Symbole, TableElement[]> table;
    private boolean conflictTable;
    private boolean conflictAnalyse;



    public TableAnalyseLR0(CalculCollection calculCollection, Map<Symbole, Set<Symbole>> suivants, boolean conflict){
        // calculCollection: object de type CalculCollection stockant tous les items calculés d'une grammaire
        // suivants: Set<String> stockant tous les suivants de la grammaire traitées dans calculCollection
        this.suivants = suivants;
        // on récupère les items et transitions de notre object calculCollection
        this.items = calculCollection.getItems();
        this.transitions = calculCollection.getTransitions();
        this.table = new HashMap<>();
        this.conflictTable = conflict;
    }

    @Override
    public Map<Symbole, TableElement[]> getTable() {
        return this.table;
    }

    @Override
    public boolean getConflict() {
        return this.conflictTable;
    }

    @Override
    public boolean getConflictAnalyse() {
        return this.conflictAnalyse;
    }

    @Override
    public void fillTable(){
        if (!conflictTable){
            initTable();
            // pour chaque transition, on vérifie si la clé est un terminal ou non
            for (Transition transition : this.transitions.keySet()){
                int itemIndex = this.transitions.get(transition);
                Symbole transitionSymbole = transition.getTerminal();
                // si c'est un non terminal, on ajoute le numéro d'item vers lequel il pointe
                if (transitionSymbole.isNonTerminal()){
                    this.table.get(transitionSymbole)[transition.getItemId()] = new Element(Integer.toString(itemIndex));
                }
                else{
                    this.table.get(transitionSymbole)[transition.getItemId()] = new Element("d" + itemIndex);
                }
            }
            for (Transition transition : this.transitions.keySet()){
                for (ItemPoint itemPoint : transition.getValue()){
                    if (itemPoint.fini() || itemPoint.getRegle().getRegle().contains(EPSYLONE)){
                        // si la clé d'un itemPoint est S', on ajoute la valeur acceptante
                        if (itemPoint.getNonTerminal().equals(APOSTROPHE)){
                            this.table.get(DOLLAR)[transitions.get(transition)] = new Element("acc");
                        }
                        // sinon on ajoute sa réduction à tous les suivants de sa clé
                        else{
                            for (Symbole suivant : this.suivants.get(itemPoint.getNonTerminal())){
                                if (this.table.get(suivant)!=null){
                                    if (this.table.get(suivant)[transitions.get(transition)]!=null){
                                        if (!this.table.get(suivant)[transitions.get(transition)].containsReduction(itemPoint.getReduction())){
                                            TableElement element;
                                            if (this.table.get(suivant)[transitions.get(transition)] instanceof Element){
                                                element = this.table.get(suivant)[transitions.get(transition)];
                                                element.addElement(itemPoint.getReduction());
                                            }
                                            else{
                                                element = new Element(itemPoint.getReduction().toString());
                                            }
                                            this.table.get(suivant)[transitions.get(transition)] = element;
                                            this.conflictAnalyse = true;
                                        }
                                    }
                                    else{
                                        this.table.get(suivant)[transitions.get(transition)] = itemPoint.getReduction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            formatTable();
        }
    }

    private void initTable(){
        // Initialise la table en créant pour chaque symbole une array de String
        for (Transition transition : this.transitions.keySet()){
            this.table.putIfAbsent(transition.getTerminal(), new TableElement[this.items.size()]);
        }
        // le symbole $ n'étant pas dans les transitions, on l'ajoute manuellement
        this.table.put(DOLLAR, new TableElement[this.items.size()]);
    }

    private void formatTable(){
        Set<Symbole> minuscule = new TreeSet<>(Comparator.comparing(String::valueOf));
        Set<Symbole> majuscule = new HashSet<>();
        for (Symbole symbole : this.table.keySet()){
            if (!symbole.equals(DOLLAR)){
                if (symbole.isTerminal()){
                    minuscule.add(symbole);
                }
                else if (!symbole.equals(Grammaire.firstNonTerminal)){
                    majuscule.add(symbole);
                }
            }
        }
        // on tri les minuscules et majuscules pour afficher dans le tableau
        // les minuscules, $ puis les majuscules.
        List<Symbole> sortedMajuscule = new ArrayList<>();
        sortedMajuscule.add(Grammaire.firstNonTerminal);
        sortedMajuscule.addAll(majuscule);
        List<Symbole> finalSortedTerminal = new ArrayList<>(minuscule);
        finalSortedTerminal.add(DOLLAR);
        finalSortedTerminal.addAll(sortedMajuscule);

        Map<Symbole, TableElement[]> sortedTable = new LinkedHashMap<>();
        for (Symbole key : finalSortedTerminal){
            sortedTable.put(key, this.table.get(key));
        }
        this.table = sortedTable;
    }

    public String toString(){
        // size correspond à la taille d'une case du tableau dans l'affichage
        int size = 13;

        String res = "";
        for (int i = 0; i<size; i++){
            res += " ";
        }
        for (Symbole terminal : this.table.keySet()){
            res += terminal;
            for (int i = 0; i<size-terminal.getValue().length(); i++){
                res += " ";
            }
        }
        res += "\n";
        for (int i = 0; i<this.items.size(); i++){
            res += i;
            for (int j = 0; j<size - Integer.toString(i).length(); j++){
                res += " ";
            }
            for (Symbole terminal : this.table.keySet()){
                if (this.table.get(terminal)!=null){
                    TableElement e = this.table.get(terminal)[i];
                    if (Objects.nonNull(e)) {
                        String text = e.toString();
                        int len = text.length();
                        res += text;
                        for (int l = 0; l < size - len; l++) {
                            res += " ";
                        }
                    }
                        // si le text est null, on décide de ne rien afficher
                    else{
                        for (int j = 0; j<size; j++){
                            res += " ";
                        }
                    }
                }
            }
            res += "\n";
        }
        return res;
    }
}
