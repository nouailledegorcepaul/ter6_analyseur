package views;

import calcul.Container;
import controller.Controller;
import grammaire.Grammaire;
import calcul.*;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.io.IOException;
import java.util.*;

import static grammaire.Grammaire.DOLLAR;

public class Results implements InteractiveView {

    @FXML
    public VBox itemsVbox;
    public TableView<Container> analyseTableView;
    public TableView<Analyse> analyseChaineTableView;
    public BorderPane borderPane;
    public TextField analyseTextField;
    public Button analyseButton;

    public Grammaire grammaire;
    public Label grammaireLabel, labelSuivants, labelPremiers;
    public AnchorPane premiersContainer, suivantsContainer;

    public Button nextItemButton;
    public Button previousItemButton;
    public Button resetItemButton;
    private Scene scene;
    private Controller controller;
    private CalculCollection collection;
    private TableAnalyse tableAnalyse;
    private Analyseur analyseur;
    private List<Label> itemsLabel;
    private int itemIndex;
    private String type;
    private boolean conflict;


    public static Results creerVue() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Results.class.getClassLoader().getResource("analyse_view.fxml"));
        fxmlLoader.load();
        Results results = fxmlLoader.getController();
        results.initialiserVue();
        return results;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public Scene getScene() {
        return scene;
    }

    private void initialiserVue() {
        this.scene = new Scene(borderPane, 640, 480);
    }

    public void writeData(Grammaire grammaire, String type, boolean conflict){
        this.grammaire = grammaire;
        this.type = type;
        this.conflict = conflict;
        grammaireLabel.setText("Grammaire : "+grammaire.toString());
        this.itemsLabel = new ArrayList<>();
        this.itemsVbox.getChildren().clear();
        this.analyseTableView.getColumns().clear();
        this.analyseTextField.clear();
        this.analyseChaineTableView.getItems().clear();
        this.analyseChaineTableView.getColumns().clear();

        switch (type) {
            case "LR1" -> {
                this.collection = new CalculCollectionLR1(this.grammaire, conflict);
                this.tableAnalyse = new TableAnalyseLR1(this.collection, this.collection.hasFoundConflict() && conflict);
                premiersContainer.setVisible(false);
                suivantsContainer.setVisible(false);
            }
            case "LR0" -> {
                this.collection = new CalculCollectionLR0(this.grammaire, conflict);
                this.tableAnalyse = new TableAnalyseLR0(this.collection, this.grammaire.getSuivants(), this.collection.hasFoundConflict() && conflict);
                premiersContainer.setVisible(true);
                suivantsContainer.setVisible(true);
                writePremiers();
                writeSuivants();
            }
        }
        this.collection.calculateAllItems();
        this.tableAnalyse.fillTable();
        writeGrammaire();
        writeItems();
        writeAnalyseTable();
        this.analyseur = new Analyseur(this.grammaire.getLangage(), this.tableAnalyse, this.tableAnalyse.getConflictAnalyse());
        initAnalyseChaineTable();
    }

    public void writePremiers(){
        Map<Symbole, Set<Symbole>> premiers = this.grammaire.getPremiers();
        StringBuilder res = new StringBuilder();
        for (Symbole symbole : premiers.keySet()){
            Label label = new Label("Premiers("+symbole+")="+premiers.get(symbole));
            label.setFont(new Font(12.0));
            res.append("Premiers(").append(symbole).append(")=").append(premiers.get(symbole)).append("\n");
        }
        labelPremiers.setText(String.valueOf(res));
    }

    public void writeSuivants(){
        Map<Symbole, Set<Symbole>> suivants = this.grammaire.getSuivants();
        StringBuilder res = new StringBuilder();
        for (Symbole symbole : suivants.keySet()){
            Label label = new Label("Suivants("+symbole+")="+suivants.get(symbole));
            label.setFont(new Font(12.0));
            res.append("Suivants(").append(symbole).append(")=").append(suivants.get(symbole)).append("\n");
        }
        labelSuivants.setText(String.valueOf(res));
    }

    public void writeGrammaire(){
        StringBuilder res = new StringBuilder();
        for (String s : this.grammaire.getGrammaireBrut()){
            Label label = new Label(s);
            label.setFont(new Font(14.0));
            res.append(s).append("\n");
        }
        grammaireLabel.setText(res.toString());
    }

    public void writeItems(){
        Map<Integer, Set<ItemPoint>> items = this.collection.getItems();

        StringBuilder item0String = new StringBuilder("I0 = {");
        List<ItemPoint> l = items.get(0).stream().toList();
        for (int i = 0; i<l.size(); i++){
            item0String.append(l.get(i));
            if (i < l.size()-1) {
                item0String.append(", ");
            }
        }
        item0String.append("}");

        Label item0 = new Label(item0String.toString());
        item0.setFont(new Font(14.0));
        this.itemsLabel.add(item0);

        int currentItem = 0;
        Map<Transition, Integer> transitions = this.collection.getTransitions();
        for (Transition transition : transitions.keySet()){
            Label label = new Label();
            if (transitions.get(transition) > currentItem){
                label.setText("I"+transitions.get(transition) + " = " + transition.detailedToString());
                currentItem++;
            }
            else{
                label.setText(transition + " = I" + transitions.get(transition));
            }
            label.setFont(new Font(14.0));
            label.setMinWidth(Region.USE_PREF_SIZE);
            this.itemsLabel.add(label);
        }
        startItems();
    }

    public void nextItem(){
        if (this.itemIndex < this.itemsLabel.size()){
            this.itemsVbox.getChildren().add(this.itemsLabel.get(this.itemIndex));
            this.itemIndex++;

            this.nextItemButton.setDisable(this.itemIndex == this.itemsLabel.size());
            this.previousItemButton.setDisable(this.itemIndex == 1);
        }
    }

    public void previousItem(){
        if (this.itemIndex > 1){
            this.itemsVbox.getChildren().remove(this.itemIndex-1);
            this.itemIndex--;

            this.previousItemButton.setDisable(this.itemIndex == 1);
            this.nextItemButton.setDisable(this.itemIndex == this.itemsLabel.size());
        }
    }

    public void startItems(){
        this.itemIndex = 0;
        this.itemsVbox.getChildren().clear();
        this.nextItemButton.setDisable(false);
        this.previousItemButton.setDisable(true);
        nextItem();
    }

    public void writeAnalyseTable(){

        Map<Symbole, TableElement[]> table = this.tableAnalyse.getTable();
        if (!this.tableAnalyse.getConflict()){
            ObservableList<Container> data = FXCollections.observableArrayList();
            for (int i=0; i<table.get(DOLLAR).length; i++){
                List<String> l = new ArrayList<>();
                for (Symbole key : table.keySet()) {
                    if (table.get(key)!=null){
                        if (table.get(key)[i] == null){
                            l.add(null);
                        }
                        else{
                            l.add(table.get(key)[i].toString());
                        }
                    }
                }
                Container container = new Container(String.valueOf(i), l);
                data.add(container);
            }
            // Les valeurs contenues dans data sont correctes. Probablement une erreur dans l'assignation ci-dessous.
            TableColumn<Container, String> numColumn = new TableColumn<>("Item N°");
            numColumn.setCellValueFactory(new PropertyValueFactory<>("itemNum"));
            this.analyseTableView.getColumns().add(numColumn);
            for (int i = 0; i<table.keySet().size(); i++){
                Symbole key = table.keySet().stream().toList().get(i);
                TableColumn<Container, String> column = new TableColumn<>(key.getValue());
                column.setCellValueFactory(new PropertyValueFactory<>("value" + i));
                this.analyseTableView.getColumns().add(column);
            }
            this.analyseTableView.getItems().setAll(data);
            this.analyseTableView.setFixedCellSize(25);
            this.analyseTableView.prefHeightProperty().bind(this.analyseTableView.fixedCellSizeProperty().multiply(Bindings.size(this.analyseTableView.getItems()).add(1.01)));
            this.analyseTableView.minHeightProperty().bind(this.analyseTableView.prefHeightProperty());
            this.analyseTableView.maxHeightProperty().bind(this.analyseTableView.prefHeightProperty());
            this.analyseTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        }
        else{
            this.analyseTableView.getItems().clear();
        }
    }

    public void initAnalyseChaineTable(){
        TableColumn<Analyse, String> pileColumn = new TableColumn<>("Pile");
        pileColumn.setCellValueFactory(new PropertyValueFactory<>("pile"));

        TableColumn<Analyse, String> chaineColumn = new TableColumn<>("Chaine");
        chaineColumn.setCellValueFactory(new PropertyValueFactory<>("chaineStr"));

        TableColumn<Analyse, String> actionColumn = new TableColumn<>("Action");
        actionColumn.setCellValueFactory(new PropertyValueFactory<>("action"));

        this.analyseChaineTableView.getColumns().addAll(pileColumn, chaineColumn, actionColumn);
    }

    public void onAnalyseClick(){
        String chaine = this.analyseTextField.getText();
        this.analyseur.analyse(chaine);
        List<Analyse> analyses = this.analyseur.getAnalyses();

        this.analyseChaineTableView.getItems().setAll(analyses);
        this.analyseChaineTableView.setFixedCellSize(25);
        this.analyseChaineTableView.prefHeightProperty().bind(this.analyseChaineTableView.fixedCellSizeProperty().multiply(Bindings.size(this.analyseChaineTableView.getItems()).add(1.01)));
        this.analyseChaineTableView.minHeightProperty().bind(this.analyseChaineTableView.prefHeightProperty());
        this.analyseChaineTableView.maxHeightProperty().bind(this.analyseChaineTableView.prefHeightProperty());
        this.analyseTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }

    public void newItemsWindow() {
        try {
            this.controller.showCollectionItems(this.grammaire, this.type, this.conflict);
        } catch (Exception e) {
            System.out.println("Cant load new window");
            e.printStackTrace();
        }
    }

    public void newTableAnalyseWindow() {
        try {
            this.controller.showTableAnalyse(this.tableAnalyse);
        } catch (Exception e) {
            System.out.println("Cant load new window");
            e.printStackTrace();
        }
    }

    public void toMainMenu() {
        this.controller.showMainMenu();
    }
}