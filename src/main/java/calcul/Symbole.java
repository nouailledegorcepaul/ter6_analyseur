package calcul;

public class Symbole {
    // Modélise un terminal ou non terminal
    private final boolean terminal;
    private final String value;

    public Symbole(String value, boolean isTerminal){
        this.value = value;
        this.terminal = isTerminal;
    }

    public boolean isTerminal(){
        return this.terminal;
    }

    public boolean isNonTerminal(){
        return !this.terminal;
    }

    public String getValue(){
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Symbole s)) {
            return false;
        }
        return terminal == s.isTerminal() && value.equals(s.getValue());
    }

    // Surcharge de la méthode hashCode
    @Override
    public int hashCode() {
        int result = 17;
        int boolInt = terminal ? 1 : 0;
        result = 31 * result + boolInt;
        result = 31 * result + value.hashCode();
        return result;
    }

    @Override
    public String toString(){
        return this.value;
    }
}
