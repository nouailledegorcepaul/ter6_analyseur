package calcul;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static grammaire.Grammaire.EPSYLONE;

public class ItemPointLR1 implements ItemPoint{
    // Modélisation d'une règle à parcourir lors du calcul des items.
    public int position, maxPosition;
    public Symbole nonTerminal;
    public Regle regle;
    public Set<Symbole> end;
    public boolean initial;

    public ItemPointLR1(Symbole nonTerminal, Regle regle, boolean initial){
        // nonTerminal: String représentant le non terminal associé à une règle
        // regle: String représentant la règle associée au non terminal
        // position: int indiquant la position du point dans l'analyse de la règle
        // maxPosition: int indiquant la position maximale du point, permettant de savoir si l'analyse est terminée ou non
        // Exemple: new ItemPoint("S", "aB") représente [S -> .aB]

        this.nonTerminal = nonTerminal;
        // On nettoie la règle en supprimant epsilon
        this.regle = regle;
        this.position = 0;
        this.maxPosition = this.regle.getRegle().size();
        this.end = new HashSet<>();
        this.initial = initial;
    }

    public ItemPointLR1(ItemPoint itemPoint){
        // Constructeur de copie
        this.nonTerminal = itemPoint.getNonTerminal();
        this.regle = itemPoint.getRegle();
        this.position = itemPoint.getPosition();
        this.maxPosition = itemPoint.getMaxPosition();
        this.end = itemPoint.getEnd();
    }

    @Override
    public Symbole getNonTerminal() {
        return nonTerminal;
    }

    @Override
    public Regle getRegle() {
        return regle;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public int getMaxPosition() {
        return maxPosition;
    }

    @Override
    public Set<Symbole> getEnd() {
        return end;
    }

    // Incrémente la position du point
    public void update(){ this.position++; }

    // Précise si le point est au bout de la règle
    public boolean fini(){
        return position == maxPosition || regle.getRegle().contains(EPSYLONE);
    }

    // Précise si la règle à un suivant
    public boolean hasSuivant(){
        return getSuivant()!=null;
    }

    // Renvoie le suivant s'il existe
    public Symbole getSuivant(){
        if (!fini()){
            return this.regle.getRegle().get(position);
        }
        return null;
    }

    public Reduction getReduction(){
        return new Reduction(this.nonTerminal, this.regle);
    }

//    public Set<String> getLR1Suivants() {
//        Set<String> characters = new HashSet<>();
//        for (int i = position+1; i<this.maxPosition; i++){
//            if (Character.isUpperCase(this.regle.charAt(i))) {
//                if (this.regle.charAt(i) != '$') {
//                    characters.add(String.valueOf(this.regle.charAt(i)));
//                }
//            }
//        }
//        characters.addAll(this.end);
//        return characters;
//    }

    public boolean equalsLR1(ItemPoint itemPoint) {
        // si les itemPoint sont identiques hors fin
        if (position == itemPoint.getPosition() && regle.equals(itemPoint.getRegle()) && nonTerminal.equals(itemPoint.getNonTerminal())){
            // on verifie que leurs fins sont différentes l'une de l'autre
            return true;
        }
        return false;
    }

    // Surcharge de la méthode equals
    // Deux ItemPoint sont égaux si la position de leurs points, leurs règles et leurs non terminaux sont égaux
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ItemPointLR1 ip)) {
            return false;
        }
        return position == ip.position && regle.equals(ip.regle) && nonTerminal.equals(ip.nonTerminal) && end.containsAll(ip.end) && ip.end.containsAll(end);
    }

    // Surcharge de la méthode hashCode
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + position;
        result = 31 * result + maxPosition;
        result = 31 * result + nonTerminal.hashCode();
        result = 31 * result + regle.hashCode();
        result = 31 * result + end.hashCode();
        return result;
    }

    @Override
    public String tableToString(){
        StringBuilder r = new StringBuilder();
        for (int i = 0; i<regle.getRegle().size(); i++){
            r.append(regle.getRegle().get(i));
        }
        return "[" + nonTerminal + " → " + r + "]";
    }

    public String toString(){
        StringBuilder r = new StringBuilder();
        boolean point = false;
        for (int i = 0; i<regle.getRegle().size(); i++){
            if (position==i){
                point = true;
                r.append(".");
            }
            r.append(regle.getRegle().get(i));
        }
        if (!point){
            r.append(".");
        }
        r.append(", ");
        List<Symbole> endArray = this.end.stream().toList();
        for (int i = 0; i<endArray.size(); i++){
             r.append(endArray.get(i));
             if (i<this.end.size()-1){
                 r.append("/");
             }
        }
        return "[" + nonTerminal + " → " + r + "]";
    }
}
