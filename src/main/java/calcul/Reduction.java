package calcul;

public class Reduction implements TableElement{
    // Modélise une reduction dans la table d'analyse
    private Symbole nonTerminal;
    private Regle regle;

    public Reduction(Symbole symbole, Regle regle){
        this.nonTerminal = symbole;
        this.regle = regle;
    }

    public Symbole getNonTerminal(){
        return this.nonTerminal;
    }

    public Regle getRegle(){
        return this.regle;
    }

    public String toString(){
        return "r("+nonTerminal+ "->" + regle.tableToString() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Reduction r)) {
            return false;
        }
        return nonTerminal == r.getNonTerminal() && regle.equals(r.getRegle());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + nonTerminal.hashCode();
        result = 31 * result + regle.hashCode();
        return result;
    }

    @Override
    public boolean isAcc() {
        return false;
    }

    @Override
    public boolean isDform() {
        return false;
    }

    @Override
    public boolean containsReduction(Reduction reduction) {
        return this.equals(reduction);
    }

    @Override
    public void addElement(TableElement element) {

    }
}
