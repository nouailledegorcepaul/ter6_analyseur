package grammaire;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GrammaireFormatter {

    public static Map<String, List<String>> format(List<String> grammaire){
        // transforme un language ecrit sous forme reelle en une structure de donnees specifique
        Map<String, List<String>> res = new HashMap<>();
        for (String line : grammaire){
            if (!line.isBlank()){
                // on supprime les espaces
                line = line.replaceAll(" ", "");
                // on separe les deux elements de la ligne
                String[] separation = line.split("->");
                String key = separation[0];
                String value = separation[1];
                String[] value2 = value.split("\\|");

                // on ajoute les valeurs aux terminaux correspondants
                res.putIfAbsent(key, new ArrayList<>());
                for (String v : value2){
                    res.get(key).add(v);
                }
            }
        }
        return res;
    }
}
