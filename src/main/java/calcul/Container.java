package calcul;

import java.util.List;

public class Container {
    // classe Container servant à stocker les données dans les TableView JavaFX
    public String[] values;
    public String itemNum;
    public Container(String itemNum, List<String> val){
        this.values = new String[20];
        this.itemNum = itemNum;
        for (int i=0; i<val.size(); i++){
            if (val.get(i)!=null){
                this.values[i] = val.get(i);
            }
            else{
                this.values[i] = "-";
            }
        }
    }
    public String getItemNum(){return itemNum;}
    public String getValue0(){
        return values[0];
    }
    public String getValue1(){
        return values[1];
    }
    public String getValue2(){
        return values[2];
    }
    public String getValue3(){
        return values[3];
    }
    public String getValue4(){
        return values[4];
    }
    public String getValue5(){
        return values[5];
    }
    public String getValue6(){
        return values[6];
    }
    public String getValue7(){
        return values[7];
    }
    public String getValue8(){
        return values[8];
    }
    public String getValue9(){
        return values[9];
    }
    public String getValue10(){
        return values[10];
    }

}