package calcul;

public interface TableElement {

    boolean isAcc();
    boolean isDform();
    boolean containsReduction(Reduction reduction);
    void addElement(TableElement element);
}
