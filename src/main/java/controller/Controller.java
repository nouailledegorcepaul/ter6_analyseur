package controller;

import calcul.TableAnalyse;
import grammaire.Grammaire;
import views.ViewManager;


public class Controller {

    private final ViewManager viewManager;

    public Controller(ViewManager viewManager) {
        this.viewManager = viewManager;
        this.viewManager.setController(this);
    }
    public void run() {
        viewManager.show();
    }

    public void showMainMenu(){
        viewManager.showMainMenu();
    }

    public void showResults(){
        viewManager.showResults();
    }

    public void showCollectionItems(Grammaire grammaire, String type, boolean conflict){
        viewManager.showCollectionItems();
        viewManager.setCollectionItem(grammaire, type, conflict);
    }

    public void showTableAnalyse(TableAnalyse tableAnalyse) {
        viewManager.setTableAnalyse(tableAnalyse);
        viewManager.showTableAnalyse();
    }


    public void setGrammaire(Grammaire grammaire, String type, boolean conflict){
        this.viewManager.setGrammaire(grammaire, type, conflict);
    }
}
