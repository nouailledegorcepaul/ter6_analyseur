package calcul;

import java.util.ArrayList;
import java.util.List;

public class Pile {
    // Modélise la pile lors de l'analyse d'une chaine
    private List<PileElement> pile;

    public Pile(){
        this.pile = new ArrayList<>();
        this.pile.add(new PileNombre(0));
    }

    public List<PileElement> getPile(){
        return this.pile;
    }

    public void setPile(List<PileElement> p){
        this.pile = new ArrayList<>(p);
    }
    public PileNombre getSommet(){
        return (PileNombre)this.pile.get(this.pile.size()-1);
    }

    public void removeSommet(){
        if (this.pile.size()>0){
            this.pile.remove(this.pile.size()-1);
        }
    }

    public void addElement(PileElement element){
        this.pile.add(element);
    }

    public Pile conflictPile(){
        // Créé une pile affichant le message d'erreur de conflit
        Pile res = new Pile();
        res.getPile().clear();
        res.getPile().add(new PileSymbole(new Symbole("ERROR: CONFLICT", true)));
        return res;
    }

    public Pile erreurPile(Symbole symbole){
        // Créé une pile affichant le message d'erreur de symbole non présent dans l'alphabet
        Pile res = new Pile();
        res.getPile().clear();

        res.getPile().add(new PileSymbole(new Symbole("ERROR: " + symbole.getValue(), true)));
        return res;
    }

    public String toString(){
        String res = "";
        for (PileElement e : pile){
            res += e.toString();
        }
        return res;
    }
}
