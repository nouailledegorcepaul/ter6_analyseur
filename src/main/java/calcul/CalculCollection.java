package calcul;

import java.util.Map;
import java.util.Set;

public interface CalculCollection {

    Map<Integer, Set<ItemPoint>> getItems();
    Map<Transition, Integer> getTransitions();
    void calculateAllItems();
    boolean hasFoundConflict();
}
