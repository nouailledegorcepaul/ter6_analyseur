package views;

import calcul.TableAnalyse;
import controller.Controller;
import grammaire.Grammaire;
import javafx.stage.Stage;

import java.io.IOException;

public class ViewManager implements InteractiveView {
    private final Stage stage;
    private Results results;
    private MainMenu mainMenu;
    private CollectionItemWindow collectionItemWindow;
    private TableAnalyseScene tableAnalyseWindow;
    private Controller controller;

    public ViewManager(Stage stage) throws IOException {
        this.stage = stage;
        initialisationVues();
    }

    private void initialisationVues() throws IOException {
        this.mainMenu = MainMenu.creerVue();
        this.results = Results.creerVue();
        this.collectionItemWindow = CollectionItemWindow.creerVue();
        this.tableAnalyseWindow = TableAnalyseScene.creerVue();
    }

    public void show() {
        this.stage.setScene(mainMenu.getScene());
        this.stage.setTitle("Entrez une grammaire");
        this.stage.show();
    }

    public void showMainMenu(){
        this.stage.setScene(mainMenu.getScene());
        this.stage.setTitle("Entrez une grammaire");
        this.stage.show();
    }

    public void showResults() {
        this.stage.setScene(results.getScene());
        this.stage.setTitle("Analyse ascendante");
        this.stage.show();
    }

    public void showCollectionItems() {
        Stage stage = new Stage();
        stage.setScene(collectionItemWindow.getScene());
        stage.setTitle("Collection d'items");
        stage.show();
    }

    public void showTableAnalyse() {
        Stage stage = new Stage();
        stage.setScene(tableAnalyseWindow.getScene());
        stage.setTitle("Table d'analyse");
        stage.show();
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
        mainMenu.setController(controller);
        results.setController(controller);
        collectionItemWindow.setController(controller);
        tableAnalyseWindow.setController(controller);
    }

    public void setGrammaire(Grammaire grammaire, String type, boolean conflict){
        this.results.writeData(grammaire, type, conflict);
    }

    public void setCollectionItem(Grammaire grammaire, String type, boolean conflict) {
        this.collectionItemWindow.writeData(grammaire, type, conflict);
    }

    public void setTableAnalyse(TableAnalyse tableAnalyse) {
        this.tableAnalyseWindow.writeAnalyseTable(tableAnalyse);
    }
}
