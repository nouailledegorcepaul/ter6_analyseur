package calcul;

public class PileSymbole implements PileElement{
    // Modélise un symbole dans la pile
    private final Symbole value;

    public PileSymbole(Symbole value){
        this.value = value;
    }

    public Symbole getValue(){
        return this.value;
    }

    public String toString(){
        return this.value.getValue();
    }
}
