package views;

import controller.Controller;
import exceptions.SyntaxArrowMissing;
import grammaire.Grammaire;
import grammaire.ReadFile;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class MainMenu implements InteractiveView{
    @FXML
    public TextArea grammaireTextArea;
    public TextArea alphabetTextArea;
    public VBox vboxScene;
    public RadioButton lr0Button;
    public RadioButton lr1Button;
    public CheckBox conflictBox;

    final FileChooser fileChooser = new FileChooser();
    private Grammaire grammaire;
    private Scene scene;
    private Controller controller;
    private String type;

    public static MainMenu creerVue() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainMenu.class.getClassLoader().getResource("main_menu.fxml"));
        fxmlLoader.load();
        MainMenu mainMenu = fxmlLoader.getController();
        mainMenu.initialiserVue();
        return mainMenu;
    }

    private void initialiserVue() {
        ToggleGroup group = new ToggleGroup();
        lr0Button.setToggleGroup(group);
        lr1Button.setToggleGroup(group);
        lr0Button.setSelected(true);
        type = "LR0";
        group.selectedToggleProperty().addListener((observableValue, toggle1, toggle2) -> {
            RadioButton chosen = (RadioButton) toggle1.getToggleGroup().getSelectedToggle();
            type = chosen.getText();
        });

        this.scene = new Scene(vboxScene, 640, 480);
        alphabetTextArea.setPromptText("Exemple: a,b,c,sap,et"); //to set the hint text
        alphabetTextArea.getParent().requestFocus();
        grammaireTextArea.setPromptText("Exemple: S -> S a | b c | ε"); //to set the hint text
        grammaireTextArea.getParent().requestFocus();
        setupFileChooser();
    }
    public Scene getScene() {
        return scene;
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @FXML
    private void onGrammaireClick() throws SyntaxArrowMissing {
        String stringAlphabet = alphabetTextArea.getText();
        String stringGrammaire = grammaireTextArea.getText();
        List<String> langageList = Arrays.stream(stringAlphabet.split("\n")).toList();
        List<String> grammaireList = Arrays.stream(stringGrammaire.split("\n")).toList();
        this.grammaire = new Grammaire(langageList, grammaireList);
        this.controller.setGrammaire(this.grammaire, this.type, this.conflictBox.isSelected());
        this.controller.showResults();
    }

    @FXML
    private void onFileChooserClick() {
        File file = fileChooser.showOpenDialog(null);
        if (file == null) return;
        String pathname = file.getAbsolutePath();
        ReadFile fileReader = new ReadFile(pathname);
        StringBuilder content = new StringBuilder();
        this.alphabetTextArea.setText(fileReader.getGrammaireBrut().get(0));
        for (int i = 1; i < fileReader.getGrammaireBrut().size(); i++) {
            content.append(fileReader.getGrammaireBrut().get(i)).append("\n");
        }
        this.grammaireTextArea.setText(content.toString());
    }

    public Grammaire getGrammaire(){
        return this.grammaire;
    }

    private void setupFileChooser() {
        fileChooser.setTitle("Choisir un fichier contenant une grammaire");
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        fileChooser.setInitialDirectory(new File(currentPath));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Fichier texte (*.txt)", "*.txt")
        );
    }
}