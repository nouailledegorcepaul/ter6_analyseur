package calcul;

import java.util.Set;

public interface ItemPoint {

    Symbole getNonTerminal();
    Regle getRegle();
    int getPosition();
    int getMaxPosition();
    Set<Symbole> getEnd();

    // Met à jour de le point d'analyse
    void update();

    // Précise si le point est au bout de la règle
    boolean fini();

    // Précise si la règle à un suivant
    boolean hasSuivant();

    // Renvoie le suivant s'il existe
    Symbole getSuivant();

    // Renvoie la représentation de la réduction
    Reduction getReduction();

    String tableToString();
    boolean equalsLR1(ItemPoint itemPoint);

}
