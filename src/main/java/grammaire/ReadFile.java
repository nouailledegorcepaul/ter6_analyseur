package grammaire;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadFile {

    private final String filename;
    private final List<String> grammaireBrut;

    public ReadFile(String filename) {
        this.filename = filename;
        this.grammaireBrut = new ArrayList<>();
        ReadGrammaire();
    }

    private void ReadGrammaire() {
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                grammaireBrut.add(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public List<String> getGrammaireBrut() {
        return grammaireBrut;
    }
}
