package calcul;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static grammaire.Grammaire.DOLLAR;
import static grammaire.Grammaire.EPSYLONE;

public class Analyseur {
    // Classe Analyseur permet d'analyser une chaine
    private List<Analyse> analyses;
    private Pile currentPile;
    List<Symbole> currentChaine;
    private final Map<Symbole, TableElement[]> table;
    private final boolean conflict;
    private final Set<Symbole> alphabet;

    public Analyseur(Set<Symbole> alphabet, TableAnalyse tableAnalyse, boolean conflict){
        // alphabet: Set<Symbole> représentant l'ensemble des symboles contenus dans l'alphabet de la grammaire
        // tableAnalyse: la table d'analyse calculée au préalable afin de pouvoir analyser la chaine
        // conflict: boolean précisant si la table d'analyse présente un ou plusieurs conflits.
        this.alphabet = alphabet;
        this.table = tableAnalyse.getTable();
        this.analyses = new ArrayList<>();
        if (conflict){
            this.conflict = true;
        }
        else{
            this.conflict = tableAnalyse.getConflict();
        }
    }

    public List<Analyse> getAnalyses(){
        return this.analyses;
    }

    public void analyse(String chaine){
        // fonction d'initialisation de l'analyse d'une chaine

        // On vide la liste d'analyse dans le cas où un analyse a déjà été faite avant.
        this.analyses.clear();
        // On initialise la pile et on formatte la chaine pour la transformer en symbole
        this.currentPile = new Pile();
        this.currentChaine = stringToSymbole(chaine);
        analyseChaine();
    }

    public List<Symbole> stringToSymbole(String chaine){
        // Convertit un String en List<Symbole>
        // Exemple: chaine=asapba, alphabet={a,b, sap}
        // Return: [a, sap, b, a]
        List<Symbole> res = new ArrayList<>();
        String value = "";
        for (char c : chaine.toCharArray()){
            // Afin de parser correctement l'alphabet, on concatene les char jusqu'à
            // ce que la valeur soit présente dans l'alphabet
            value += String.valueOf(c);
            // Chaque symbole conforme est un terminal
            Symbole current = new Symbole(value, true);
            if (alphabet.contains(current)){
                // la valeur est présente dans l'alphabet. On ajoute le symbole au résultat
                // et on vide la chaine
                res.add(current);
                value = "";
            }
        }
        // Ajout systématique du symbole dollar à la fin de la chaine
        res.add(DOLLAR);
        return res;
    }


    private void analyseChaine() {
        // Si un conflict est présent, on stoppe l'analyse et on ajoute seulement une
        // ligne d'analyse indiquant que l'analyse n'est pas faisable.
        if (this.conflict){
            // On créé une analyse ayant pour valeur CONFLICT IN TABLE
            List<Symbole> err = new ArrayList<>();
            err.add(new Symbole("IN", false));
            Analyse error = new Analyse(this.currentPile.conflictPile(), err);
            error.setAction(new Element("TABLE"));
            this.analyses.add(error);
        }
        // Si aucun conflit n'est trouvée, alors on débute l'analyse
        else{
            Analyse lastAnalyse = new Analyse(new Pile(), new ArrayList<>());
            // Dans le cas où une analyse précédent est disponible, on la récupère
            if (this.analyses.size()>0){
                lastAnalyse = this.analyses.get(this.analyses.size() - 1);
            }
            // si la dernière action faite n'est pas acc
            if (this.analyses.size()==0 || !lastAnalyse.getAction().equals("acc")) {
                // on crée une nouvelle analyse de la pile et de la chaine actuelle
                Analyse analyse = new Analyse(this.currentPile, this.currentChaine);
                // on récupere le premier symbole de la chaine à traiter
                // ainsi que le numéro de transition qui se trouve en fin de pile
                Symbole firstChaine = analyse.getFirstChaine();
                PileNombre sommetPile = analyse.getSommetPile();
                // Si le symbole est présent dans l'alphabet (colonnes de la table)
                if (this.table.containsKey(firstChaine)){
                    TableElement action = this.table.get(firstChaine)[sommetPile.getValue()];
                    // si l'action n'est pas nulle, on assigne cette action à l'objet
                    if (action != null) {
                        analyse.setAction(action);
                        // si l'action est différent du cas d'acceptation acc
                        if (!action.isAcc()) {
                            // si l'action est de la forme d + numéro d'item
                            // Exemple: d1, d2, d10, d12 etc...
                            if (action.isDform()) {
                                Element element = (Element) action;
                                this.currentPile.addElement(new PileSymbole(firstChaine));
                                // le cast en int ne renverra jamais d'exception dans ce cas précis donc aucune vérification n'est faite.
                                this.currentPile.addElement(new PileNombre(Integer.parseInt(element.getValue().replace("d", ""))));
                                this.currentChaine.remove(0);
                            }
                            // sinon, l'action est forcément une réduction
                            else {
                                Reduction reduction = (Reduction) action;
                                // on récupère la règle associée à la réduction, puis on soustrait les terminaux et non terminaux
                                // de celle ci à la pile grâce à la fonction updatePileWithReduction
                                Regle regle = reduction.getRegle();
                                updatePileWithReduction(regle);
                                // on met à jour le nouveau sommet de pile
                                sommetPile = this.currentPile.getSommet();
                                // on ajoute le nonTerminal de la réduction ainsi que sa valeur dans la table d'analyse
                                Symbole nonTerminal = reduction.getNonTerminal();
                                Element num = (Element)this.table.get(nonTerminal)[sommetPile.getValue()];
                                this.currentPile.addElement(new PileSymbole(nonTerminal));
                                this.currentPile.addElement(new PileNombre(Integer.parseInt(num.getValue())));
                            }
                        }
                        // l'objet Analyse est complet
                        // la pile et la chaine en cours sont elles prêtes pour le traitement de la prochaine analyse
                        this.analyses.add(analyse);
                        analyseChaine();
                    }
                    // si l'action est nulle, il y a blocage dans l'analyse et on arrête
                    else {
                        analyse.setAction(new Element("Blocage !"));
                        this.analyses.add(analyse);
                    }
                }
                // Le symbole ne se trouve pas dans l'alphabet, on supprime alors la table et on précise
                // quel symbole pose problème.
                else{
                    this.analyses.clear();
                    List<Symbole> err = new ArrayList<>();
                    err.add(new Symbole("NOT IN", false));
                    Analyse error = new Analyse(this.currentPile.erreurPile(firstChaine), err);
                    error.setAction(new Element("GRAMMAIRE"));
                    this.analyses.add(error);
                }
            }
        }

    }

    private void updatePileWithReduction(Regle regle){
        // met à jour la pile avec une règle de réduction en supprimant les
        // terminaux et non terminaux de celle-ci du sommet de pile
        // Exemple: regle = Sa, pile = 0S1a3
        // Renvoie: pile = 0
        Pile newPile = this.currentPile;
        regle.getRegle().remove(EPSYLONE);
        int symbolesToRemove = regle.getRegle().size();
        // pour chaque symbole à supprimer, on supprime ce symbole ET son numéro d'item associé
        for (int i = 0; i<symbolesToRemove; i++){
            newPile.removeSommet();
            newPile.removeSommet();
        }
        this.currentPile = newPile;
    }

    public String toString(){
        int maxLenPile = 0;
        int maxLenChaine = 0;
        for (Analyse analyse : this.analyses){
            int pileLen = analyse.getPile().getPile().size();
            int chaineLen = analyse.getChaine().size();
            if (pileLen > maxLenPile){
                maxLenPile = pileLen;
            }
            if (chaineLen > maxLenChaine){
                maxLenChaine = chaineLen;
            }
        }
        // ajout du padding entre chaque colonne, ici 3
        maxLenChaine+=3;
        maxLenPile+=3;
        String res = "Pile";
        for (int i = 0; i < maxLenPile-4; i++){
            res += " ";
        }
        res += "Chaine";
        for (int i = 0; i < maxLenChaine-6; i++){
            res += " ";
        }
        res += "Action\n";
        for (Analyse analyse : this.analyses){
            int pileLen = analyse.getPile().getPile().size();
            int chaineLen = analyse.getChaine().size();
            res+=analyse.getPile();
            for (int i = 0; i<maxLenPile - pileLen; i++){
                res+=" ";
            }
            res+=analyse.getChaine();
            for (int i = 0; i<maxLenChaine - chaineLen; i++){
                res+=" ";
            }
            res+=analyse.getAction()+"\n";
        }
        return res;
    }
}
