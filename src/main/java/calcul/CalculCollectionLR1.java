package calcul;

import grammaire.Grammaire;
import java.util.*;

import static grammaire.Grammaire.APOSTROPHE;
import static grammaire.Grammaire.EPSYLONE;

public class CalculCollectionLR1 implements CalculCollection{

    private final Grammaire grammaire;
    private final Map<Integer, Set<ItemPoint>> items;
    private final Map<Symbole, Set<ItemPoint>> baseItems;
    private final Map<Symbole, Set<Symbole>> premiers;
    private final Map<Transition, Integer> transitions;
    private int itemIndex, itemIndexToAdd;
    private final boolean conflict;
    private boolean working;

    public CalculCollectionLR1(Grammaire grammaire, boolean conflict){
        // grammaire: la version formattée et utilisable pour le calcul de la collection
        // itemIndex: le numéro d'item sur lequel on travaille
        // itemIndexToAdd: le numéro du nouvel item à ajouter
        // items: le dictionnaire stockant tous les items calculés, ayant pour clé son numero d'item et pour valeur l'ensemble de ses règles
        // baseItems: le dictionnaire stockant toutes les règles de base d'un non terminal, ayant pour clé le non terminal et pour valeur ses règles
        // transitions: le dictionnaire stockant toutes les transitions calculées, ayant pour clé une transition, et pour valeur le numero d'item associé à sa valeur
        // conflict: boolean précisant s'il faut s'arréter si un conflit est trouvé
        this.grammaire = grammaire;
        this.itemIndex = 0;
        this.itemIndexToAdd = 0;
        this.items = new HashMap<>();
        this.baseItems = new HashMap<>();
        this.transitions = new TreeMap<>(new TransitionComparator());
        this.premiers = this.grammaire.getPremiers();
        this.conflict = conflict;
        this.working = true;
    }

    @Override
    public Map<Integer, Set<ItemPoint>> getItems() {
        return items;
    }

    @Override
    public Map<Transition, Integer> getTransitions() {
        return transitions;
    }

    @Override
    public boolean hasFoundConflict() {
        return !working;
    }

    @Override
    public void calculateAllItems(){
        // on initie les règles de base de chaque non terminal
        for (Symbole symbole : this.grammaire.getGrammaire().keySet()){
            initItems(symbole);
        }
        // initialise le premier item de la collection avec la grammaire augmentée S' -> S
        initI0();
        // itemIndex initialisé à 0 puisqu'on commence par calculer les items de I0
        this.itemIndex = 0;
        // itemIndex initialisé à 1 puisque I0 existe déjà, il faut donner calculer I1
        this.itemIndexToAdd = 1;
        calculItems();

    }

    private void initItems(Symbole symbole){
        // Stocke dans une map ayant pour clé un String non terminal et pour valeur un Set de ItemPoint.
        // les règles de base d'un non terminal
        this.baseItems.put(symbole, new HashSet<>());
        for (Regle regle : this.grammaire.getGrammaire().get(symbole)){
            this.baseItems.get(symbole).add(new ItemPointLR1(symbole, regle, false));
        }
    }

    private void initI0(){
        // On ajoute la grammaire augmentée S'->S aux règles de base de S
        // pour créer le premier item I0
        this.items.put(0, new HashSet<>());
        Symbole initSymbole = APOSTROPHE;
        List<Symbole> symboles = new ArrayList<>();
        symboles.add(Grammaire.firstNonTerminal);
        Regle initRegle = new Regle(symboles);
        ItemPoint start = new ItemPointLR1(initSymbole, initRegle, true);
        start.getEnd().add(new Symbole("$", true));

        Set<ItemPoint> e = getDevelopment(start, new HashSet<>());
        Transition res = new Transition(0, EPSYLONE, sortItem(e));
        if (this.conflict){
            checkConflict(res);
        }
        this.items.get(0).clear();
        this.items.get(0).addAll(sortItem(e));
    }

    private void calculItems(){
        // On initialise un ensemble de String ayant un paramètre de tri qui tri dans l'ordre alphabétique décroissant
        // On y ajoute l'ensemble des suivants
        List<Symbole> suivants = new ArrayList<>(getSuivants(this.itemIndex));
        // Pour chaque suivant, on calcul sa transition
        for (Symbole suivant : suivants){
            if (this.working){
                Transition transition = getTransition(this.itemIndex, suivant);
                if (this.working){
                    boolean alreadyIn = false;
                    // Après avoir obtenu la transition, on vérifie si ses valeurs n'appartiennent pas déjà
                    // à un item de la collection d'item
                    for (int index : this.items.keySet()){
                        // Si les valeurs de transition sont déjà présentes, on l'ajoute aux transitions
                        // avec pour valeur l'indice de l'item ayant ces memes valeurs.
                        if (this.items.get(index).equals(transition.getValue())){
                            this.transitions.put(transition, index);
                            alreadyIn = true;
                            break;
                        }
                    }
                    if (!alreadyIn){
                        // Si les valeurs de transitions ne sont pas déjà présentes, on l'ajoute aux transitions
                        // avec pour valeur le nouvel indice d'item à ajouter
                        // On ajoute ensuite le nouvel item aux items.
                        this.transitions.put(transition, this.itemIndexToAdd);
                        this.items.put(this.itemIndexToAdd, transition.getValue());
                        // On incrémente l'indice d'item à ajouter.
                        this.itemIndexToAdd++;
                    }
                }
            }

        }
        // On calcul les nouveaux items en prenant connaissance de l'item suivant
        if (this.itemIndex < this.itemIndexToAdd - 1 && this.working){
            this.itemIndex++;
            calculItems();
        }
    }

    private List<Symbole> getSuivants(int itemNum){
        Set<Symbole> suivants = new TreeSet<>(Comparator.comparing(String::valueOf));
        // Pour chaque règle, on ajoute son suivant à l'ensemble s'il en a un
        for (ItemPoint ip : this.items.get(itemNum)){
            if (ip.hasSuivant()){
                if (!ip.getSuivant().equals(EPSYLONE)){
                    suivants.add(ip.getSuivant());
                }
            }
        }
        List<Symbole> listSuivants = new ArrayList<>(suivants);
        if (listSuivants.contains(Grammaire.firstNonTerminal)){
            List<Symbole> sortedSuivants = new ArrayList<>();
            sortedSuivants.add(Grammaire.firstNonTerminal);
            for (Symbole s : listSuivants){
                if (!s.equals(Grammaire.firstNonTerminal)){
                    sortedSuivants.add(s);
                }
            }
            return sortedSuivants;
        }
        return listSuivants;
    }

    private Transition getTransition(int itemNum, Symbole suivant){
        Set<ItemPoint> updatedItemPoints = new HashSet<>();
        // Pour chaque regle, si son suivant est égal à celui donné en paramètre
        // on crée une copie de l'objet, on met à jour son point d'analyse et on l'ajoute
        // à l'ensemble des nouvelles regles.
        for (ItemPoint i : this.items.get(itemNum)){
            if (i.hasSuivant()){
                if (i.getSuivant().equals(suivant)) {
                    ItemPoint i2 = new ItemPointLR1(i);
                    // avancement du point d'analyse
                    i2.update();
                    updatedItemPoints.add(i2);
                }
            }
        }
        Set<ItemPoint> finalSet = new HashSet<>();
        for (ItemPoint i : updatedItemPoints){
            finalSet.addAll(getDevelopment(i, new HashSet<>()));
        }
        Transition res = new Transition(itemNum, suivant, sortItem(finalSet));
        if (this.conflict){
            checkConflict(res);
        }
        return res;
    }

    private void checkConflict(Transition transition){
        // Met à jour le boolean working qui permet de préciser si on calcule le reste de la collection.
        // On test si la transition donnée en paramètre présente ou non des conflits.

        // On stocke tous les items finis dans un ensemble
        Set<ItemPoint> endItems = new HashSet<>();
        for (ItemPoint item : transition.getValue()){
            if (item.fini()){
                endItems.add(item);
            }
        }
        Set<Symbole> terminaux = new HashSet<>();
        for (ItemPoint item : endItems){
            if (!item.getNonTerminal().equals(APOSTROPHE) && item.fini()){
                Set<Symbole> suivs = item.getEnd();
                for (ItemPoint item2 : transition.getValue()){
                    if (!item2.fini()){
                        Symbole next = item2.getRegle().getSymbole(item2.getPosition());
                        if (suivs.contains(next)){
                            System.out.println("Conflict réduire décaler: " + transition.getTerminal() +", "+item);
                            working = false;
                        }
                    }
                }
            }
            for (Symbole end : item.getEnd()){
                if (terminaux.contains(end)){
                    System.out.println("Conflict réduire réduire: " + transition.getTerminal() +", "+item + ", " + end);
                    working = false;
                }
            }
            terminaux.addAll(item.getEnd());
        }
    }

    private Set<ItemPoint> sortItem(Set<ItemPoint> set) {
        Set<ItemPoint> finalItem = new HashSet<>();
        Set<ItemPoint> itemAnalyzed = new HashSet<>();
        for (ItemPoint itemPoint : set){
            if (!itemAnalyzed.contains(itemPoint)) {
                ItemPoint newPoint = new ItemPointLR1(itemPoint);
                for (ItemPoint itemPoint2 : set) {
                    if (!itemAnalyzed.contains(itemPoint2)) {
                        if (!itemPoint.equals(itemPoint2)) {
                            if (itemPoint.equalsLR1(itemPoint2)) {
                                newPoint.getEnd().addAll(itemPoint2.getEnd());
                                itemAnalyzed.add(itemPoint2);
                            }
                        }
                    }
                }
                finalItem.add(newPoint);
            }
        }
        return finalItem;
    }

    private Set<ItemPoint> getDevelopment(ItemPoint itemPoint, Set<ItemPoint> res){
        // développe un itemPoint
        res.add(itemPoint);
        if (itemPoint.hasSuivant()){
            Symbole suivant = itemPoint.getSuivant();
            if (suivant.isNonTerminal()){
                Set<Symbole> suivantsLR1 = getLR1Premiers(itemPoint);
                Set<ItemPoint> developpedItemPoint = this.baseItems.get(suivant);
                Set<ItemPoint> updatedItemPoint = new HashSet<>();
                for (ItemPoint item : developpedItemPoint){
                    ItemPoint newItem = new ItemPointLR1(item.getNonTerminal(), item.getRegle(), false);
                    newItem.getEnd().addAll(suivantsLR1);
                    updatedItemPoint.add(newItem);
                }

                for (ItemPoint item : updatedItemPoint){
                    if (!res.contains(item)){
                        res.addAll(getDevelopment(item, res));
                    }
                }
            }
        }
        return res;
    }

    private Set<Symbole> getLR1Premiers(ItemPoint item) {
        if (item.getPosition()+1 < item.getMaxPosition()){
            Set<Symbole> symboles = new HashSet<>();
            Symbole symbole = item.getRegle().getSymbole(item.getPosition()+1);
            if (symbole.isNonTerminal()){
                symboles.addAll(this.premiers.get(symbole));
                Set<Symbole> symbolesToAdd = new HashSet<>();
                for (Symbole s : symboles){
                    if (s.equals(EPSYLONE)){
                        symbolesToAdd.addAll(item.getEnd());
                        break;
                    }
                }
                symboles.addAll(symbolesToAdd);
            }
            else{
                symboles.add(symbole);
            }
            return symboles;
        }
        else{
            return item.getEnd();
        }
    }
}
