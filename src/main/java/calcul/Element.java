package calcul;

public class Element implements TableElement{
    // Classe Element modélisant un élément de la table d'analyse, permettant de stocker plusieurs valeurs pour
    // les conflicts.
    private String value;
    private boolean acc;

    public Element(String value){
        this.value = value;
        acc = this.value.equals("acc");
    }

    public String getValue(){
        return this.value;
    }

    @Override
    public boolean isAcc(){
        return this.acc;
    }

    @Override
    public boolean isDform() {
        return String.valueOf(this.value.charAt(0)).equals("d");
    }

    @Override
    public boolean containsReduction(Reduction reduction) {
        return value.contains(reduction.toString());
    }

    public void addElement(TableElement element){
        this.value += " / " + element.toString();
    }

    public String toString(){
        return this.value;
    }


}
