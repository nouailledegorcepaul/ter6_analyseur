package calcul;

import java.util.ArrayList;
import java.util.List;

public class Analyse {
    // Modélisation d'une analyse de chaine à un instant t
    private Pile pile;
    private String action;
    private String chaineStr;
    private List<Symbole> chaine;

    public Analyse(Pile pile, List<Symbole> chaine){
        // pile: String représentant l'état de la pile lors de l'analyse de la chaine
        // chaine: String représentant l'état de la chaine lors de son analyse
        this.pile = new Pile();
        this.pile.setPile(pile.getPile());
        this.chaine = new ArrayList<>(chaine);
        this.chaineStr = chaineToString();
    }

    public Pile getPile(){
        return this.pile;
    }

    public List<Symbole> getChaine(){
        return this.chaine;
    }

    public String getAction(){
        return this.action;
    }

    // Ne pas supprimer, utilisé pour préciser que chaineStr est un PropertyValueFactory
    public String getChaineStr(){
        return this.chaineStr;
    }

    public PileNombre getSommetPile(){
        // Afin d'obtenir le sommet de pile, on parcourt la pile à partir du sommet
        // et récupère toutes les valeurs présentes jusqu'à tomber sur une lettre.
        return this.pile.getSommet();
    }

    public Symbole getFirstChaine(){return chaine.get(0);}

    public void setAction(TableElement action){
        this.action = action.toString();
    }

    private String chaineToString(){
        StringBuilder res = new StringBuilder();
        for (Symbole s : chaine){
            res.append(s.getValue());
        }
        return res.toString();
    }
    public String toString(){
        return pile + " | " +  chaineToString() + " | " + action;
    }

}
