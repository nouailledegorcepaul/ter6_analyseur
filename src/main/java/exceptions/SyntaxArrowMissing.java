package exceptions;

public class SyntaxArrowMissing extends Exception{
    public SyntaxArrowMissing(String errorMessage){
        super(errorMessage);
    }
}
