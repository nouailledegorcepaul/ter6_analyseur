package views;

import calcul.Container;
import calcul.Symbole;
import calcul.TableAnalyse;
import calcul.TableElement;
import controller.Controller;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static grammaire.Grammaire.DOLLAR;


public class TableAnalyseScene implements InteractiveView{

    private Scene scene;

    @FXML
    public AnchorPane anchorPane;
    public TableView<Container> analyseTable;

    public static TableAnalyseScene creerVue() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(TableAnalyseScene.class.getClassLoader().getResource("table_analyse.fxml"));
        fxmlLoader.load();
        TableAnalyseScene scene = fxmlLoader.getController();
        scene.initialiserVue();
        return scene;
    }

    @Override
    public void setController(Controller controller) {
    }

    public Scene getScene() {
        return scene;
    }

    private void initialiserVue() {
        this.scene = new Scene(anchorPane, 640, 480);
    }

    public void writeAnalyseTable(TableAnalyse tableAnalyse){
        Map<Symbole, TableElement[]> table = tableAnalyse.getTable();
        if (!tableAnalyse.getConflict()){
            ObservableList<Container> data = FXCollections.observableArrayList();
            for (int i=0; i<table.get(DOLLAR).length; i++){
                List<String> l = new ArrayList<>();
                for (Symbole key : table.keySet()) {
                    if (table.get(key)!=null){
                        if (table.get(key)[i] == null){
                            l.add(null);
                        }
                        else{
                            l.add(table.get(key)[i].toString());
                        }
                    }
                }
                Container container = new Container(String.valueOf(i), l);
                data.add(container);
            }
            // Les valeurs contenues dans data sont correctes. Probablement une erreur dans l'assignation ci-dessous.
            TableColumn<Container, String> numColumn = new TableColumn<>("Item N°");
            numColumn.setCellValueFactory(new PropertyValueFactory<>("itemNum"));
            this.analyseTable.getColumns().add(numColumn);
            for (int i = 0; i<table.keySet().size(); i++){
                Symbole key = table.keySet().stream().toList().get(i);
                TableColumn<Container, String> column = new TableColumn<>(key.getValue());
                column.setCellValueFactory(new PropertyValueFactory<>("value" + i));
                this.analyseTable.getColumns().add(column);
            }
            this.analyseTable.getItems().setAll(data);
            this.analyseTable.setFixedCellSize(25);
            this.analyseTable.prefHeightProperty().bind(this.analyseTable.fixedCellSizeProperty().multiply(Bindings.size(this.analyseTable.getItems()).add(1.01)));
            this.analyseTable.minHeightProperty().bind(this.analyseTable.prefHeightProperty());
            this.analyseTable.maxHeightProperty().bind(this.analyseTable.prefHeightProperty());
            this.analyseTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        }
        else{
            this.analyseTable.getItems().clear();
        }
    }
}