package app;

import controller.Controller;
import javafx.application.Application;
import javafx.stage.Stage;
import views.ViewManager;

import java.io.IOException;


public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        ViewManager viewManager = new ViewManager(stage);
        Controller controller = new Controller(viewManager);
        controller.run();
    }
    public static void main(String[] args) {launch();}

}
