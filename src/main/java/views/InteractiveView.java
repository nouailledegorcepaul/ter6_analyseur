package views;

import controller.Controller;

public interface InteractiveView {
    void setController(Controller controleur);
}
