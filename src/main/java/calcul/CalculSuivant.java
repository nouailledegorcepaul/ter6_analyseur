package calcul;

import grammaire.GrammaireFormatter;

import java.util.*;

public class CalculSuivant {

    public Map<String, List<String>> grammaire;
    public Map<String, Set<String>> suivants;
    public Map<String, Set<String>> premiers;

    public CalculSuivant(List<String> grammaireBrut){
        CalculPremiers calculPremiers = new CalculPremiers(grammaireBrut);
        this.premiers = calculPremiers.getAllPremiers();

        this.grammaire = GrammaireFormatter.format(grammaireBrut);
        this.suivants = getSuivants();
    }

    public CalculSuivant(List<String> grammaireBrut, Map<String, Set<String>> premiers){
        this.premiers = premiers;

        grammaire = GrammaireFormatter.format(grammaireBrut);
        this.suivants = getSuivants();
    }

    public Map<String, Set<String>> getSuivants(){
        Map<String, Set<String>> suivants = new HashMap<>();
        for (String nonTerminal : grammaire.keySet()){
            System.out.println("Suivants pour: " + nonTerminal);
            suivants.putIfAbsent(nonTerminal, new HashSet<>());
            if (nonTerminal.equals("S")){
                suivants.get("S").add("$");
            }
            List<String> termGram = grammaire.get(nonTerminal);
            for (String val : termGram) {
                // on separe la cle de sa valeur en separant en plus chaque valeur si un "|" est present
                for (int i=0; i<val.length(); i++){
                    char char1 = val.charAt(i);
                    if (Character.isUpperCase(char1)){
                        for (String v2 : grammaire.get(String.valueOf(char1))){
                            String v3 = val.replace(String.valueOf(char1), v2);
                            for (int j = 0; j < v3.length() - 1; j++) {
                                char char2 = v3.charAt(j);
                                char char3 = v3.charAt(j + 1);
                                if (Character.isUpperCase(char2)) {
                                    suivants.putIfAbsent(String.valueOf(char2), new HashSet<>());
                                    if (!Character.isUpperCase(char3)) {
                                        suivants.get(String.valueOf(char2)).add(String.valueOf(char3));
                                    }
                                    else{
                                        suivants.get(String.valueOf(char2)).addAll(this.premiers.get(char3 +""));
                                    }
                                }
                                if (Character.isUpperCase(char2) && nonTerminal.equals("S") && i+2==val.length()){
                                    suivants.putIfAbsent(String.valueOf(char2), new HashSet<>());
                                    suivants.get(String.valueOf(char2)).add("$");
                                }
                            }
                        }
                    }
                }
            }
        }
        return suivants;
    }
}
