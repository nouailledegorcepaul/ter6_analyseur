package calcul;

import java.util.ArrayList;
import java.util.List;

import static grammaire.Grammaire.EPSYLONE;

public class Regle {
    // Modélise une règle composée de Symbole
    private List<Symbole> regle;

    public Regle(List<Symbole> regle){
        this.regle = regle;
    }

    public List<Symbole> getRegle(){
        return this.regle;
    }

    public Symbole getSymbole(int i){
        return this.regle.get(i);
    }
    public Symbole getFinChaine(){
        return this.getSymbole(this.regle.size()-1);
    }

    public Symbole getDebutChaine(){
        return this.getSymbole(0);
    }

    public void replaceSymbole(Symbole symbole, Regle newRegle){
        // Remplace tous les symboles symbole par la regle newRegle
        List<Symbole> symboles = new ArrayList<>();
        for (Symbole s : this.regle) {
            if (s.equals(symbole)) {
                symboles.addAll(newRegle.getRegle());
            } else {
                symboles.add(s);
            }
        }
        this.regle = symboles;
    }

    public String tableToString(){
        String res = "";
        for (Symbole s : this.regle){
            res += s.toString();
        }
        return res;
    }

    public String toString(){
        String res = "";
        for (Symbole s : this.regle){
            if (!s.equals(EPSYLONE)){
                res += s.toString();
            }
        }
        return res;
    }
}
