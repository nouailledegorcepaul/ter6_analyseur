module app {
    requires javafx.controls;
    requires javafx.fxml;

    opens views to javafx.fxml;
    exports app;
    exports calcul;
    exports grammaire;
}