package calcul;

import java.util.Set;

import static grammaire.Grammaire.EPSYLONE;

public class ItemPointLR0 implements ItemPoint{
    // Modélisation d'une règle à parcourir lors du calcul des items.
    public int position, maxPosition;
    public Symbole nonTerminal;
    public Regle regle;

    public ItemPointLR0(Symbole nonTerminal, Regle regle){
        // nonTerminal: String représentant le non terminal associé à une règle
        // regle: String représentant la règle associée au non terminal
        // position: int indiquant la position du point dans l'analyse de la règle
        // maxPosition: int indiquant la position maximale du point, permettant de savoir si l'analyse est terminée ou non
        // Exemple: new ItemPoint("S", "aB") représente [S -> .aB]

        this.nonTerminal = nonTerminal;
        // On nettoie la règle en supprimant epsilon
        this.regle = regle;
        this.position = 0;
        this.maxPosition = this.regle.getRegle().size();
    }

    public ItemPointLR0(ItemPoint itemPoint){
        // Constructeur de copie
        this.nonTerminal = itemPoint.getNonTerminal();
        this.regle = itemPoint.getRegle();
        this.position = itemPoint.getPosition();
        this.maxPosition = itemPoint.getMaxPosition();
    }


    @Override
    public Symbole getNonTerminal() {
        return nonTerminal;
    }

    @Override
    public Regle getRegle() {
        return regle;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public int getMaxPosition() {
        return maxPosition;
    }

    @Override
    public Set<Symbole> getEnd() {
        return null;
    }

    // Incrémente la position du point
    public void update(){ this.position++; }

    // Précise si le point est au bout de la règle
    public boolean fini(){
        return position == maxPosition || regle.getRegle().contains(EPSYLONE);
    }

    // Précise si la règle à un suivant
    public boolean hasSuivant(){
        return getSuivant()!=null;
    }

    // Renvoie le suivant s'il existe
    public Symbole getSuivant(){
        if (!fini()){
            return this.regle.getRegle().get(position);
        }
        return null;
    }

    public Reduction getReduction(){
        return new Reduction(this.nonTerminal, this.regle);
    }

    @Override
    public boolean equalsLR1(ItemPoint itemPoint) {
        return false;
    }

    // Surcharge de la méthode equals
    // Deux ItemPoint sont égaux si la position de leurs points, leurs règles et leurs non terminaux sont égaux
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ItemPointLR0 ip)) {
            return false;
        }
        return position == ip.position && regle.equals(ip.regle) && nonTerminal.equals(ip.nonTerminal);
    }

    // Surcharge de la méthode hashCode
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + position;
        result = 31 * result + maxPosition;
        result = 31 * result + nonTerminal.hashCode();
        result = 31 * result + regle.hashCode();
        return result;
    }

    @Override
    public String tableToString(){
        return "[" + nonTerminal + " → " + this.regle.tableToString() + "]";
    }

    public String toString(){
        StringBuilder r = new StringBuilder();
        boolean point = false;
        for (int i = 0; i<regle.getRegle().size(); i++){
            if (position==i){
                point = true;
                r.append(".");
            }
            if (!regle.getRegle().get(i).equals(EPSYLONE)){
                r.append(regle.getRegle().get(i));
            }
        }
        if (!point){
            r.append(".");
        }
        return "[" + nonTerminal + " -> " + r + "]";
    }
}
