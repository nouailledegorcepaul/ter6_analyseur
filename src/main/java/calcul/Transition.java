package calcul;

import java.util.List;
import java.util.Set;

public class Transition {
    // Modélisation d'une transition lors du calcul des items.
    private int itemId;
    private Symbole terminal;
    private Set<ItemPoint> value;

    public Transition(int itemId, Symbole terminal, Set<ItemPoint> value){
        // itemId: int représentant le numéro d'item sur laquelle la transition sera effectuée
        // terminal: String représentant le terminal (ou non terminal) avec laquelle la transition sera calculée
        // value: Set<ItemPoint> stockant l'ensemble des ItemPoint résultant de la transition

        //Exemple: I0 = {[S' -> .S], [S -> .aB]}
        //Exemple: Set<ItemPoint> s = resultat du calcul de transtion (de taille n)
        //new Transition(0, new Symbole("S"), s) représente Tr(I0, "S") -> { s[0], s[1], .... s[n]}

        this.itemId = itemId;
        this.terminal = terminal;
        this.value = value;
    }
    public int getItemId(){
        return this.itemId;
    }
    public Symbole getTerminal(){
        return this.terminal;
    }
    public Set<ItemPoint> getValue(){
        return this.value;
    }
    // Surcharge de la méthode equals
    // Deux Transition sont égales si leurs valeurs sont les mêmes
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Transition tr)) {
            return false;
        }
        return value.containsAll(tr.value) && tr.value.containsAll(value);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + itemId;
        result = 31 * result + value.hashCode();
        result = 31 * result + terminal.hashCode();
        return result;
    }

    public String toString(){
        return "Tr(I" + itemId +"," + terminal +")";
    }

    public String detailedToString(){
        // méthode d'affichage d'une transition avec l'intégralité de ses valeurs.
        StringBuilder values = new StringBuilder("{");
        List<ItemPoint> l = value.stream().toList();
        for (int i = 0; i<l.size(); i++){
            values.append(l.get(i).toString());
            if (i < l.size()-1) {
                values.append(", ");
            }
        }
        values.append("}");
        return "Tr(I" + itemId +"," + terminal +")="+values;
    }
}
