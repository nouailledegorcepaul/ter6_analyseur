package calcul;

public class PileNombre implements PileElement{
    // Modélise les valeurs numériques de la pile (numéro d'item)
    private final int value;

    public PileNombre(int value){
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }

    public String toString(){
        return Integer.toString(value);
    }
}
