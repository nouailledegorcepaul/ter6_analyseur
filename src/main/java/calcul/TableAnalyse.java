package calcul;

import java.util.Map;

public interface TableAnalyse {

    Map<Symbole, TableElement[]> getTable();
    boolean getConflict();
    boolean getConflictAnalyse();
    void fillTable();
}
